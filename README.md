# Dotfiles

# install dotbot

```shell
pip install dotbot
dotbot -d ~/.dotfiles -c ~/.dotfiles/dotbot.yaml
```

# tools

fzf
ack
