runtime! partials/plugins.vim
runtime! partials/settings.vim
runtime! partials/keybindings.vim
runtime! partials/encodings.vim
runtime! partials/statusline.vim

runtime! partials/colorscheme.vim
